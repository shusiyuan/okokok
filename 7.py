# -*- coding:utf-8 -*-
from selenium import webdriver

driver = webdriver.Chrome()
driver.get('https://www.sogou.com/')
cookies = driver.get_cookies()
print '删除前cookies：',cookies
driver.add_cookie({'name':'key-name', 'value':'value-123456'})
print 'cookie信息：',driver.get_cookies()
driver.delete_cookie('key-name')
# driver.delete_all_cookies()
print '删除后cookies：',driver.get_cookies()

